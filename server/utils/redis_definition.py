import json
import aioredis


async def cache_definition(k, v=None):
    redis = aioredis.from_url("redis://localhost", decode_responses=True)
    if v:
        val = json.dumps(v)
        await redis.set(f"{k}", val)
    else:
        value = await redis.get(k)
        if value:
            return json.loads(value)
        return None

