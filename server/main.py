import socket
from os import environ
from typing import Optional
from fastapi import FastAPI
from utils.redis_definition import cache_definition

from dico import dicoclient
from dico.dicoclient import DicoClient

app = FastAPI()

DICO_HOST = environ.get("DICO_URL")
dc = DicoClient(DICO_HOST)


@app.get("/search/")
async def read_root(q: Optional[str] = None):
    if not q:
        return {"error": "nothing"}
    q = q.replace('"', "")
    result = ""
    try:
        dc.open(DICO_HOST)
        dc.timeout = 100
        databases = dc.show_databases()["databases"]
        strategies = dc.show_strategies()["strategies"]
        # print(databases, strategies, sep="\n\n")
        database = databases[1][0]
        strategy = strategies[3][0]
        value = await cache_definition(f"search_{q}")
        result = value if value else dc.match(database, strategy, q)
        # if not value:
        await cache_definition(f"search_{q}", result)
    except (socket.timeout, socket.error, dicoclient.DicoNotConnectedError):
        print("Error ")
    finally:
        dc.close()
    return {q: result}


@app.get("/define/{word}")
async def get_definition(word: str = ""):
    result = ""
    try:
        dc.open(DICO_HOST)
        dc.timeout = 200
        databases = dc.show_databases()["databases"]
        strategies = dc.show_strategies()["strategies"]
        database = databases[1][0]
        strategy = strategies[3][0]
        value = await cache_definition(f"def_{word}")
        result = value if value else dc.define(database, word)
        # if not value:
        await cache_definition(f"def_{word}", result)
        # result = dc.define(database, word)
    except (socket.timeout, socket.error, dicoclient.DicoNotConnectedError):
        print("Error ")
    finally:
        dc.close()
    return {word: result}


# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: Optional[str] = None):
#     print("q string", q)
#     return {"item_id": item_id, "q": q}
